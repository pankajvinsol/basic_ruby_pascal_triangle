require_relative '../lib/pascal_triangle.rb'

print 'Enter a Number : '
number = gets.to_i  
puts 'Pascal Triangle :-'
PascalTriangle.generate(number) { |row| puts row }
