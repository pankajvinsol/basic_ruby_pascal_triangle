class PascalTriangle

  def self.generate(number)
    0.upto(number) do |row_number|
      row, previous_number = 1, 1
      1.upto(row_number) do |column_number|
        previous_number = previous_number * (row_number + 1 - column_number) / column_number
        row = "#{ row } #{ previous_number }"
      end
      yield(row)
    end
  end
end
